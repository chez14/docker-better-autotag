package main

import (
	"fmt"
	"log"
	"os"

	"github.com/urfave/cli/v2"
)

var (
	buildId    string = "n/a"
	buildDate  string = ""
	versionId  string = "n/a"
	commitHash string = "dev"
)

func main() {
	cli.VersionFlag = &cli.BoolFlag{
		Name:  "version",
		Usage: "Show version information.",
	}

	cli.HelpFlag = &cli.BoolFlag{
		Name:    "help",
		Aliases: []string{"h"},
		Usage:   "Show help information.",
		Hidden:  true,
	}

	app := &cli.App{
		Name:    "Docker Better Autotag",
		Usage:   "do-tag",
		Version: fmt.Sprintf("%s (Built on %s@%s (%s))", versionId, buildId, commitHash, buildDate),
		Authors: []*cli.Author{
			{
				Name:  "Chris Qiang",
				Email: "hi@christianto.net",
			},
		},
		Copyright: "©2022 chez14",

		Flags: []cli.Flag{
			&cli.BoolFlag{
				Name:    "verbose",
				Aliases: []string{"v"},
				Usage:   "Run the CLI with more infomation. Usefull for troubleshooting problems.",
			},
		},
		Commands: []*cli.Command{
			{
				Name: "run",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:    "version-string",
						Aliases: []string{"s"},
						Usage:   "Full Version as a string. Example: `v1.2.3` or 1.2.3.",
					},
					&cli.BoolFlag{
						Name:    "dry",
						Aliases: []string{"n"},
						Usage:   "Try to generate the tag, but don't actually tag the image.",
					},
				},
				Action: func(c *cli.Context) error {
					fmt.Printf("We gotchu!")

					return nil
				},
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
