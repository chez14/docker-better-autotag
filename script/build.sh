#!/bin/bash

## Exit immediately when error happens.
set -e;


PACKAGE="gitlab.com/chez14/docker-better-autotag"

BUILDID=${CI_JOB_ID:-"dev"}

BUILDDATE=$(date --rfc-3339 seconds)

VERSIONID=${CI_COMMIT_TAG:-"dev"}

COMMITHASH=${CI_COMMIT_SHORT_SHA:-$(git rev-list --abbrev-commit -n 1 HEAD)}

LDFLAGS=$(
    cat <<EOF
-X 'main.buildId=${BUILDID}'
-X 'main.buildDate=${BUILDDATE}'
-X 'main.versionId=${VERSIONID}'
-X 'main.commitHash=${COMMITHASH}'
EOF
)

echo "Running build with build-time flags: \n${LDFLAGS}\n"

go build -ldflags="${LDFLAGS}" -o build/dobeat

echo "Done.\n"
echo "chmod-ing the built binary as executable..."

chmod +x build/dobeat

echo "Done.\n"