package autotag

import (
	"context"
	"errors"
	"fmt"
	"strings"

	"github.com/docker/docker/client"
	"github.com/facette/natsort"
	"github.com/gogs/git-module"
)

type Runner struct {
	VersionString string
	Prefix        string
	WorkingFolder string
	Latest        string
	IsVerbose     bool
	IsDry         bool
	TargetImage   string

	isLatest  bool
	gitClient *git.Repository
}

// Generate a tag from the literal strings. The function will return
// tagged version with better
func generateTag(literalVersion string, prefix string) []string {
	literalVersion = strings.TrimPrefix(literalVersion, "v")
	// split the literal version, add the mwith prefix if needed.
	var tags = strings.Split(literalVersion, ".")
	var tagSize = len(tags)
	for i := 0; i < tagSize; i++ {
		var literalTag strings.Builder

		literalTag.WriteString(prefix)

		// To generate v1, v1.2, v1.2.3, etc.
		for j := 0; j < i; j++ {
			if i > 0 {
				literalTag.WriteString(".")
			}
			literalTag.WriteString(tags[j])
		}

		tags[i] = literalTag.String()
	}

	return tags
}

// Get Memoized Git Repo handler for current working directory.
func (rnr Runner) getGit() *git.Repository {
	if rnr.gitClient != nil {
		return rnr.gitClient
	}

	repo, err := git.Open(rnr.WorkingFolder)
	if err != nil {
		fmt.Println("Error: unable to open git folder " + rnr.WorkingFolder)
		panic(err)
	}

	rnr.gitClient = repo
	return repo
}

// Detect current version. If the version has already defined as a parameter,
// then we wont run the discovery via git.
func (rnr Runner) detectVersion() {
	if len(rnr.VersionString) != 0 {
		return
	}

	fmt.Println("Info: version is not defined. trying to auto-detect it via git describe")
	// run auto detect.
	describer := git.NewCommand("describe", "--tags", "--candidates 0")
	rev, err := describer.RunInDir(rnr.WorkingFolder)
	if err != nil {
		fmt.Println("Error: unable to run git describe on working folder: " + rnr.WorkingFolder)
		panic(err)
	}

	version := string(rev)
	if strings.Contains(version, "fatal: No names found") {
		fmt.Println("Error: current commits has no tag associated with it. You can provide the version manually via parameter --version-string or -s.")
		panic(errors.New("No version detected"))
	}

	rnr.VersionString = version
}

// Detect if current version is the latest version on the git. If latest status
// provided via command parameter, the automatic discovery via git will not be
// run. Additionally, the latest detection works by listing all tags available
// on the git and then we sort them. If the last tag on the sorted version list
// are the same with current detected version, the latest tag are added. Sorting
// are done with natural sort.
func (rnr Runner) detectLatest() {
	if rnr.Latest == "always" || rnr.Latest == "true" {
		rnr.isLatest = true
		return
	}

	client := rnr.getGit()
	listedTags, err := client.Tags()
	if err != nil {
		fmt.Println("Error: unable to get list of git tags")
	}

	// cleanup listed tags, removing the `v` prefixes.
	for i, _ := range listedTags {
		listedTags[i] = strings.TrimPrefix(listedTags[i], "v")
	}

	natsort.Sort(listedTags)

	rnr.isLatest = (listedTags[len(listedTags)-1] == strings.TrimPrefix(rnr.VersionString, "b"))
}

// Run the docker image tagging. Source of the image provided via parameter.
func (rnr Runner) runTagger(tags []string) {
	// runs docker auto tagger
	cli, err := client.NewClientWithOpts(client.FromEnv)
	if err != nil {
		fmt.Println("Error: Unable to create docker client from environment.")
		panic(err)
	}

	for _, tag := range tags {
		cli.ImageTag(context.Background(), rnr.TargetImage, tag)
	}
}

// Run the autotagger.
func (rnr Runner) Run() {
	rnr.detectVersion()

	var versions = generateTag(rnr.VersionString, rnr.Prefix)

	// Run latest detector
	rnr.detectLatest()

	// Apend latest if we're on the latest.
	if rnr.isLatest {
		versions = append(versions, "latest")
	}

	fmt.Println(versions)
	if rnr.IsDry {
		fmt.Println("Info: Dobeat runs in dry mode: docker tag wont be ran.")
		return
	}

	rnr.runTagger(versions)
}
